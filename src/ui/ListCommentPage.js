import React, { useContext, Memo, memo } from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    FlatList,
    Text,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import { Context } from '../controller/provider';
import { DISLIKE_ALL_POSTING, DISLIKE_POSTING, LIKE_ALL_POSTING, LIKE_POSTING, RESET_ALL_POSTING } from '../controller/action';

export default function ListPostingPage() {
    const [globalState, globalDispatch] = useContext(Context);
    return (
        <View style={styles.container}>
            <ActionAllComponent />
            <ListPostingComponent data={globalState.stateOne} />
        </View>
    );
}



const ActionAllComponent = memo(() => {
    return (
        <View style={styles.actionAll}>
            <ButtonLikeAllComponent />
            <View style={{ width: 10 }} />
            <ButtonResetAllComponent />
            <View style={{ width: 10 }} />
            <ButtonDislikeAllComponent />
        </View>
    );
})

const ButtonLikeAllComponent = memo(() => {
    const [globalState, globalDispatch] = useContext(Context);
    return (
        <TouchableOpacity onPress={() => globalDispatch({ 'type': LIKE_ALL_POSTING })} style={styles.buttonLikeAll}>
            <View>
                <Text style={{ color: 'white' }}>Like All</Text>
            </View>
        </TouchableOpacity>
    );
});

const ButtonResetAllComponent = memo(() => {
    const [globalState, globalDispatch] = useContext(Context);
    return (
        <TouchableOpacity onPress={() => globalDispatch({ 'type': RESET_ALL_POSTING })} style={styles.buttonResetAll}>
            <View>
                <Text style={{ color: 'black' }}>Reset All</Text>
            </View>
        </TouchableOpacity>

    );
});

const ButtonDislikeAllComponent = memo(() => {
    const [globalState, globalDispatch] = useContext(Context);
    return (
        <TouchableOpacity onPress={() => globalDispatch({ 'type': DISLIKE_ALL_POSTING })} style={styles.buttonDislikeAll}>
            <View>
                <Text style={{ color: 'white' }}>Dislike All</Text>
            </View>
        </TouchableOpacity>
    );
});

const ListPostingComponent = (props) => {
    return (
        <View style={styles.listPosting}>
            <FlatList
                data={props.data}
                contentContainerStyle={{ paddingBottom: 80 }}
                renderItem={({ item, index }) => {
                    let image = { uri: item.image_url };
                    console.log(`image url: ${item.image_url}`)
                    // console.log('like count:' + item.like);
                    return (
                        <ItemPostingComponent>
                            <View style={styles.imagePosting}>
                                <ImageBackground source={image} style={{
                                    flex: 1, height: 150, borderTopRightRadius: 10,borderTopLeftRadius: 10
                                }} />
                            </View >
                            <View style={styles.actionPosting}>
                                <View style={styles.actionPostingTotalLike}>
                                    <TotalLikeComponent total={item.total_like} />
                                </View>
                                <View style={styles.actionPostingLikeAndDislike}>
                                    <ButtonLikeComponent index={index} like={item.like} />
                                    <View style={styles.dividerLikeAndDislike} />
                                    <ButtonDislikeComponent index={index} dislike={item.like} />
                                </View>
                            </View>
                        </ItemPostingComponent>
                    );
                }
                }
            />
        </View>
    );
}

const ItemPostingComponent = (props) => {
    return (
        <View style={styles.itemPosting}>
            {props.children}
        </View>
    );
}

const TotalLikeComponent = memo((props) => {
    return (
        <View style={styles.totalLike}>
            <Text style={{ color: 'black' }}>{props.total} Like</Text>
        </View>
    );
});

const ButtonLikeComponent = memo((props) => {
    const [globalState, globalDispatch] = useContext(Context);
    let likeCount = props.like;
    return (
        <TouchableOpacity onPress={() => globalDispatch({ 'type': LIKE_POSTING, 'payloadId': props.index })}>
            <View style={styles.buttonLike}>
                <Text style={{ color: 'white' }}>Like</Text>
            </View>
        </TouchableOpacity>
    );
}); 

const ButtonDislikeComponent = memo((props) => {
    const [globalState, globalDispatch] = useContext(Context);
    let likeCount = props.dislike;
    return (
        <TouchableOpacity onPress={() => globalDispatch({ 'type': DISLIKE_POSTING, 'payloadId': props.index })}>
            <View style={styles.buttonDislike}>
                <Text style={{ color: 'white' }}>Dislike</Text>
            </View>
        </TouchableOpacity>
    );
});

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'grey'
    },
    actionAll: {
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingHorizontal: 10
    },
    buttonLikeAll: {
        backgroundColor: 'blue',
        borderRadius: 5,
        flex: 1,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonResetAll: {
        backgroundColor: '#EBEBEB',
        borderRadius: 5,
        flex: 1,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonDislikeAll: {
        backgroundColor: 'red',
        borderRadius: 5,
        flex: 1,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listPosting: {
        paddingStart: 10,
        paddingEnd: 10
    },
    itemPosting: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 20,
        borderRadius: 10,
        borderWidth: 0.5,
    },
    imagePosting: {
        flex: 1,
        height: 150,
        backgroundColor: '#EBEBEB',
        borderTopStartRadius: 10,
        borderTopEndRadius: 10
    },
    actionPosting: {
        backgroundColor: 'white',
        flex: 1,
        height: 50,
        borderBot: 10,
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10,
        flexDirection: 'row',
    },
    actionPostingTotalLike: {
        flex: 1,
        height: 50,
        borderBot: 10,
        borderBottomStartRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    actionPostingLikeAndDislike: {
        flex: 1,
        height: 50,
        borderBot: 10,
        borderBottomEndRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    totalLike: {
        backgroundColor: '#EBEBEB',
        borderRadius: 5,
        width: 100,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center',
        marginStart: 10
    },
    buttonLike: {
        backgroundColor: 'blue',
        borderRadius: 5,
        width: 80,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonDislike: {
        backgroundColor: 'red',
        borderRadius: 5,
        width: 80,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: 10
    },
    dividerLikeAndDislike: {
        width: 10
    }
});