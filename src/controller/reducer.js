import { DISLIKE_ALL_POSTING, DISLIKE_POSTING, LIKE_ALL_POSTING, LIKE_POSTING, RESET_ALL_POSTING } from "./action";

export var listDataPosting = [
    {
        "id": 0,
        "name": "meadow 01",
        "image_url": "https://images.unsplash.com/photo-1495107334309-fcf20504a5ab?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2940&q=80",
        "like": 0,
        "dislike": 0,
        "total_like": 0
    },
    {
        "id": 1,
        "name": "meadow 02",
        "image_url": "https://images.pexels.com/photos/269138/pexels-photo-269138.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "like": 0,
        "dislike": 0,
        "total_like": 0
    },
    {
        "id": 2,
        "name": "meadow 03",
        "image_url": "https://images.pexels.com/photos/997121/pexels-photo-997121.jpeg?auto=compress&cs=tinysrgb&w=800",
        "like": 0,
        "dislike": 0,
        "total_like": 0
    },
    {
        "id": 3,
        "name": "meadow 04",
        "image_url": "https://images.pexels.com/photos/1048039/pexels-photo-1048039.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "like": 0,
        "dislike": 0,
        "total_like": 0
    },
];


export const postingReducer = (state, action) => {
    switch (action.type) {
        case LIKE_POSTING:
            console.log('like posting');
            objIndex = state.findIndex((obj => obj.id == action.payloadId));
            state[objIndex].like = state[objIndex].like + 1;
            state[objIndex].total_like = state[objIndex].total_like + 1;
            return state;
        case DISLIKE_POSTING:
            console.log('dislike posting');
            objIndex = state.findIndex((obj => obj.id == action.payloadId));
            state[objIndex].dislike = state[objIndex].total_like > 0 ? state[objIndex].dislike + 1 : 0;
            state[objIndex].total_like = state[objIndex].total_like > 0 ? state[objIndex].total_like - 1 : 0;
            return state;
        case LIKE_ALL_POSTING:
            console.log('like all posting');
            listDataPosting.forEach((value) => {
                objIndex = state.findIndex((obj => obj.id == value.id));
                state[objIndex].like = state[objIndex].total_like + 1;
                state[objIndex].total_like = state[objIndex].total_like + 1;
                return state;
            });
            return state;
        case DISLIKE_ALL_POSTING:
            console.log('dislike all posting');
            listDataPosting.forEach((value) => {
                objIndex = state.findIndex((obj => obj.id == value.id));
                state[objIndex].dislike = state[objIndex].total_like > 0 ? state[objIndex].dislike + 1 : 0;
                state[objIndex].total_like = state[objIndex].total_like > 0 ? state[objIndex].total_like - 1 : 0;
                return state;
            });
            return state;
        case RESET_ALL_POSTING:
            console.log('reset all posting');
            listDataPosting.forEach((value) => {
                objIndex = state.findIndex((obj => obj.id == value.id));
                state[objIndex].like = 0;
                state[objIndex].dislike = 0;
                state[objIndex].total_like = 0;
                return state;
            });
            return state;

        default:
            return state;
    }
}



export function combineReducers(slices) {
    return (state, action) =>
        Object.keys(slices).reduce(
            (acc, prop) => ({
                ...acc,
                [prop]: slices[prop](acc[prop], action)
            }),
            state
        )
}