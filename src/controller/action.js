export const LIKE_POSTING = 'like_posting';
export const DISLIKE_POSTING = 'dislike_posting';
export const LIKE_ALL_POSTING = 'like_all_posting';
export const DISLIKE_ALL_POSTING = 'dislike_all_posting';
export const RESET_ALL_POSTING = 'reset_all_posting';


export const likePosting = (type, payloadId) => {
    return {
        "type": type,
        "id": payloadId
    }
}

export const dilikePosting = (type, payloadId) => {
    return {
        "type": type,
        "id": payloadId
    }
}

export const likeAllPosting = (type) => {
    return {
        "type": type
    }
}

export const dislikeAllPosting = (type) => {
    return {
        "type": type,
    }
}

export const resetAllPosting = (type) => {
    return {
        "type": type,
    }
}