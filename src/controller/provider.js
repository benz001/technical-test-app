import { createContext, useMemo, useReducer } from "react";
import { combineReducers, listDataPosting, postingReducer } from "./reducer";

const Context = createContext();

const initialState = {
    stateOne: listDataPosting,
}

const rootReducer = combineReducers({
    stateOne: postingReducer,
});

function Provider({ children }) {
    const [state, dispatch] = useReducer(rootReducer, initialState);
    const store = useMemo(() => [state, dispatch], [state]);
    return (
        <Context.Provider value={store}>
            {children}
        </Context.Provider>
    );

}

export {
    Context,
    Provider
}